#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary)
{
    // Open the dictionary file
    FILE *passPt;
    char passTemp[40];
    passPt = fopen(dictionary, "r");

    // Loop through the dictionary file, one line
    // at a time.

    while ( fgets(passTemp, 40, passPt) != NULL)
    {
    // Hash each password. Compare to the target hash.

        char *hashedDict = md5(passTemp, strlen(passTemp)-1 );
        strtok(target, "\n");

    // If they match, return the corresponding password. 

        if (!strcmp(hashedDict, target))
        {
            
            return passTemp;
        }
        free(hashedDict); 
    }
    
    // Free up memory?



    return NULL;
}


int main(int argc, char *argv[])
{
    
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *hashPt;
    char hashTemp[40];
    hashPt = fopen(argv[1], "r");

    char *temp;

    while ( fgets(hashTemp, 40, hashPt) != NULL)
    {
        temp = crackHash(hashTemp, argv[2]);
        printf("%s %s", hashTemp, temp);
        //exit(1);
    }

    // For each hash, crack it by passing it to crackHash
    
    // Display the hash along with the cracked password:
    //   5d41402abc4b2a76b9719d911017c592 hello
    
    // Close the hash file
    
    // Free up any malloc'd memory?

}
